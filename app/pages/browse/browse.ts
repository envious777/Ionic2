import { Component } from '@angular/core';
import { NavController, Page } from 'ionic-angular';

/*
  Generated class for the BrowsePage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/browse/browse.html',
})
export class BrowsePage {
  numbers = [1,2,3,4,5,6,7,8];
  constructor(private navCtrl: NavController) {
  }

}
